<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Blocklist\BlocklistInformation;
use PHPUnit\Framework\TestCase;

/**
 * BlocklistInformationTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Blocklist\BlocklistInformation
 *
 * @internal
 *
 * @small
 */
class BlocklistInformationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BlocklistInformation
	 */
	protected BlocklistInformation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testIsBlocked() : void
	{
		$this->assertTrue($this->_object->isBlocked());
	}
	
	public function testGetReason() : void
	{
		$this->assertEquals('reason : bar', $this->_object->getReason());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BlocklistInformation(true, 'reason : {foo}', ['foo' => 'bar']);
	}
	
}
