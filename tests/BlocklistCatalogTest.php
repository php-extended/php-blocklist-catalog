<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Blocklist\BlocklistCatalog;
use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use PHPUnit\Framework\TestCase;

/**
 * BlocklistCatalogTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Blocklist\BlocklistCatalog
 *
 * @internal
 *
 * @small
 */
class BlocklistCatalogTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var BlocklistCatalog
	 */
	protected BlocklistCatalog $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetAllowed() : void
	{
		$result = $this->_object->isAllowed('google.com');
		$this->assertFalse($result->isBlocked());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new BlocklistCatalog(
			$this->getMockForAbstractClass(TopLevelDomainHierarchyInterface::class),
		);
	}
	
}
