<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

/**
 * BlocklistInformation class file.
 * 
 * This class is a simple implementation of the BlocklistInformationInterface.
 * 
 * @author Anastaszor
 */
class BlocklistInformation implements BlocklistInformationInterface
{
	
	/**
	 * The blocked status.
	 * 
	 * @var boolean
	 */
	protected bool $_blocked = false;
	
	/**
	 * The blocking reason.
	 * 
	 * @var string
	 */
	protected string $_reason;
	
	/**
	 * Builds a new BlocklistInformation with the given blocked status and 
	 * blocking reason.
	 * 
	 * @param boolean $blocked
	 * @param string $reason
	 * @param array<string, string> $context
	 */
	public function __construct(bool $blocked, string $reason, array $context = [])
	{
		$this->_blocked = $blocked;
		$nctx = [];
		
		foreach($context as $k => $elem)
		{
			$nctx['{'.((string) $k).'}'] = $elem;
		}
		
		$this->_reason = \strtr($reason, $nctx);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Blocklist\BlocklistInformationInterface::isBlocked()
	 */
	public function isBlocked() : bool
	{
		return $this->_blocked;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Blocklist\BlocklistInformationInterface::getReason()
	 */
	public function getReason() : string
	{
		return $this->_reason;
	}
	
}
