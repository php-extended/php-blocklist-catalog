<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-blocklist-catalog library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Blocklist;

use PhpExtended\Tld\TopLevelDomainHierarchyInterface;
use Psr\Http\Message\UriInterface;

/**
 * BlocklistCatalog class file.
 * 
 * This class is an implementation of the BlocklistInterface based 
 * 
 * @author Anastaszor
 */
class BlocklistCatalog implements BlocklistInterface
{
	
	/**
	 * The tld hierarchy.
	 * 
	 * @var TopLevelDomainHierarchyInterface
	 */
	protected TopLevelDomainHierarchyInterface $_tldHierarchy;
	
	/**
	 * The path to the blocklist catalog.
	 * 
	 * @var string
	 */
	protected string $_blocklistDataPath;
	
	/**
	 * The cached known domains. (tld => domain list).
	 * 
	 * @var array<string, array<string, integer>>
	 */
	protected array $_domains = [];
	
	/**
	 * The tested tlds.
	 * 
	 * @var array<string, integer>
	 */
	protected array $_tested = [];
	
	/**
	 * Builds a new BlocklistCatalog with the data path.
	 * @param TopLevelDomainHierarchyInterface $tldHierarchy
	 */
	public function __construct(TopLevelDomainHierarchyInterface $tldHierarchy)
	{
		$this->_tldHierarchy = $tldHierarchy;
		$this->_blocklistDataPath = \dirname(__DIR__).\DIRECTORY_SEPARATOR.'data'.\DIRECTORY_SEPARATOR;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Blocklist\BlocklistInterface::isAllowed()
	 */
	public function isAllowed(string $domain) : BlocklistInformationInterface
	{
		$idn = \idn_to_ascii($domain, \IDNA_DEFAULT, \INTL_IDNA_VARIANT_UTS46);
		if(false === $idn)
		{
			$error = \error_get_last();
			
			return new BlocklistInformation(true, 'Failed to transform tld "{tld}" value to idn ascii : {msg}', ['tld' => $domain, 'msg' => $error['message'] ?? '']);
		}
		
		$tld = $idn;
		$pos = \mb_strrpos($idn, '.');
		if(false !== $pos)
		{
			$tld = (string) \mb_substr($idn, $pos + 1);
		}
		
		if(isset($this->_tested[$tld]))
		{
			if(isset($this->_domains[$tld][$idn]))
			{
				return new BlocklistInformation(true, 'Domain "{domain}" present in blocklist.', ['domain' => $idn]);
			}
			
			return new BlocklistInformation(false, 'Domain "{domain}" not present in blocklist.', ['domain' => $idn]);
		}
		
		$this->_tested[$tld] = 1;
		
		$filePath = $this->_blocklistDataPath.\str_replace('*', '_', $tld).'.txt';
		if(!\is_file($filePath))
		{
			return $this->getInformationForTldNotFound($tld);
		}
		
		$tldFile = \file_get_contents($filePath);
		if(false === $tldFile)
		{
			return new BlocklistInformation(true, 'Failed to get data from tld "{tld}" file.', ['tld' => $tld]);
		}
		
		foreach(\explode("\n", $tldFile) as $domain)
		{
			$this->_domains[$tld][$domain] = 1;
		}
		
		if(isset($this->_domains[$tld][$idn]))
		{
			return new BlocklistInformation(true, 'Domain "{domain}" present in blocklist.', ['domain' => $idn]);
		}
		
		return new BlocklistInformation(false, 'Domain "{domain}" not present in blocklist.', ['domain' => $idn]);
	}
	
	/**
	 * Gets the information for the tld that does not exists in the known lists.
	 * 
	 * @param string $tld
	 * @return BlocklistInformationInterface
	 */
	public function getInformationForTldNotFound(string $tld) : BlocklistInformationInterface
	{
		$utf = \idn_to_utf8($tld, \IDNA_DEFAULT, \INTL_IDNA_VARIANT_UTS46);
		if(false === $utf)
		{
			return new BlocklistInformation(true, 'Failed to transform tld "{tld}" value to utf8.', ['tld' => $tld]);
		}
		
		if($this->_tldHierarchy->isTld($utf))
		{
			return new BlocklistInformation(false, 'TLD that does not contains any blocked domain.');
		}
		
		return new BlocklistInformation(true, 'Non-existant TLD');
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Blocklist\BlocklistInterface::isUriAllowed()
	 */
	public function isUriAllowed(UriInterface $uri) : BlocklistInformationInterface
	{
		return $this->isAllowed($uri->getHost());
	}
	
}
