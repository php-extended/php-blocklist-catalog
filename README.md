# php-extended/php-blocklist-catalog

A database of blocked domains based on multiple block lists

![coverage](https://gitlab.com/php-extended/php-blocklist-catalog/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-blocklist-catalog/badges/master/coverage.svg?style=flat-square)


## Last Updated Date : 2020-06-20


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-blocklist-catalog ^8`


## Basic Usage

You may use this library the following way :

```php

use PhpExtended\Blocklist\BlocklistCatalog;

/* @var $uri \Psr\Http\Message\UriInterface */
$catalog = new BlocklistCatalog();
$result = $catalog->isUriAllowed($uri);
if(!$result->isBlocked())
{
	// do something when the domain is blocked
}

```


## License

The php code of the library is under MIT (See [license file](LICENSE)).


The lists that are given in this catalog are derived from the following lists :


- AdAway default blocklist
    - Contributions by Kicelo, Dominik Schuermann and contributors
    - LICENCE [CC Attribution 3.0](http://creativecommons.org/licenses/by/3.0/)


- AdGuard Simplified Domain Names filter
    - Ad Guard Team
    - LICENCE [GNU General Public License v3.0](http://www.gnu.org/licenses/)

- Coinhive Block lists
    - coinhive-block contributors
    - LICENSE [MIT](https://opensource.org/licenses/MIT)

- CoinBlockerLists
    - CoinBlockerLists contributors
    - LICENSE [GNU AFFERO GENERAL PUBLIC LICENSE V3](https://www.gnu.org/licenses/)

